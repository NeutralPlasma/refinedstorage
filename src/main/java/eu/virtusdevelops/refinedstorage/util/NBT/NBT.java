package eu.virtusdevelops.refinedstorage.util.NBT;

import eu.virtusdevelops.refinedstorage.RefinedStorage;
import eu.virtusdevelops.refinedstorage.data.ControllerData;
import eu.virtusdevelops.refinedstorage.data.DiskData;
import eu.virtusdevelops.virtuscore.compatibility.ServerVersion;
import net.minecraft.server.v1_15_R1.Items;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

public class NBT {
    private RefinedStorage refinedStorage;
    private Legacy legacy;
    private Current current;
    private boolean useLegacy = false;


    public NBT(RefinedStorage refinedStorage) {
        this.refinedStorage = refinedStorage;
        this.legacy = new Legacy(refinedStorage);
        this.current = new Current(refinedStorage);

        if (ServerVersion.isServerVersionBelow(ServerVersion.V1_14)) {
            this.useLegacy = true;
        }
    }

    public Integer getInt(ItemStack item, String dataContainer){
        if(useLegacy){
            return legacy.getInt(item, dataContainer);
        }else{
            return current.getInt(item, dataContainer);
        }
    }

    public String getString(ItemStack item, String dataContainer){
        if(useLegacy){
            return legacy.getString(item, dataContainer);
        }else{
            return current.getString(item, dataContainer);
        }
    }

    public ItemMeta setInt(ItemStack item, String dataContainer, int value){
        if(useLegacy){
            return legacy.setInt(item.getItemMeta(),value, dataContainer);
        }else{
            return current.setInt(item.getItemMeta(),value, dataContainer);
        }
    }

    public ItemMeta setInt(ItemMeta meta, String dataContainer, int value){
        if(useLegacy){
            return legacy.setInt(meta,value, dataContainer);
        }else{
            return current.setInt(meta,value, dataContainer);
        }
    }

    public ItemMeta setString(ItemMeta meta, String dataContainer, String value){
        if(useLegacy){
            return legacy.setString(meta, value, dataContainer);
        }else{
            return current.setString(meta, value, dataContainer);
        }
    }

    public boolean isCustomItem(ItemStack itemStack, String checkFor){
        if(getInt(itemStack, checkFor) != null){
            return true;
        }else if(getString(itemStack, checkFor) != null){
            return true;
        }
        return false;
    }

    public ItemStack createControllerItem(ItemStack itemStack, ControllerData controllerData){
        ItemMeta meta = itemStack.getItemMeta();
        meta = setInt(meta, "number", controllerData.getNumber());
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public ItemStack createControllerItem(ItemStack itemStack, int id){
        ItemMeta meta = itemStack.getItemMeta();
        meta = setInt(meta, "number", id);
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public ItemStack createDiskItem(ItemStack itemStack, DiskData diskData){
        ItemMeta meta = itemStack.getItemMeta();
        meta = setInt(meta, "number", diskData.getNumber());
        meta = setString(meta, "id", diskData.getId().toString());
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public ItemStack createDiskItem(ItemStack itemStack, int number, UUID uuid){
        ItemMeta meta = itemStack.getItemMeta();
        meta = setInt(meta, "number", number);
        meta = setString(meta, "id", uuid.toString());
        itemStack.setItemMeta(meta);
        return itemStack;
    }

}
