package eu.virtusdevelops.refinedstorage.util.NBT;

import eu.virtusdevelops.refinedstorage.RefinedStorage;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;

public class Legacy {
    private RefinedStorage refinedStorage;

    public Legacy(RefinedStorage refinedStorage){
        this.refinedStorage = refinedStorage;
    }
    @Deprecated
    public Integer getInt(ItemStack item, String dataContainer){

        NamespacedKey key = new NamespacedKey(refinedStorage, dataContainer);
        ItemMeta itemMeta = item.getItemMeta();
        CustomItemTagContainer tagContainer = itemMeta.getCustomTagContainer();
        if(tagContainer.hasCustomTag(key, ItemTagType.INTEGER)) {
            int foundValue = tagContainer.getCustomTag(key, ItemTagType.INTEGER);
            return foundValue;
        }
        return 0;
    }
    @Deprecated
    public String getString(ItemStack item, String dataContainer){
        NamespacedKey key = new NamespacedKey(refinedStorage, dataContainer);
        ItemMeta itemMeta = item.getItemMeta();
        CustomItemTagContainer tagContainer = itemMeta.getCustomTagContainer();
        if(tagContainer.hasCustomTag(key, ItemTagType.STRING)) {
            String foundValue = tagContainer.getCustomTag(key, ItemTagType.STRING);
            return foundValue;
        }
        return "none";
    }

    public ItemMeta setInt(ItemMeta meta, int number, String dataContainer){
        NamespacedKey key = new NamespacedKey(refinedStorage, dataContainer);
        try {
            CustomItemTagContainer tagContainer = meta.getCustomTagContainer();
            tagContainer.setCustomTag(key, ItemTagType.INTEGER, number);
        }catch (NullPointerException error){
            error.getCause();
            error.fillInStackTrace();
        }
        return meta;
    }

    public ItemMeta setString(ItemMeta meta, String string, String dataContainer){
        NamespacedKey key = new NamespacedKey(refinedStorage, dataContainer);
        try {
            CustomItemTagContainer tagContainer = meta.getCustomTagContainer();
            tagContainer.setCustomTag(key, ItemTagType.STRING, string);
        }catch (NullPointerException error){
            error.getCause();
            error.fillInStackTrace();
        }
        return meta;
    }



}
