package eu.virtusdevelops.refinedstorage.listeners;

import eu.virtusdevelops.refinedstorage.RefinedStorage;
import eu.virtusdevelops.refinedstorage.data.ControllerData;
import eu.virtusdevelops.refinedstorage.data.LocationData;
import eu.virtusdevelops.refinedstorage.data.SortType;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Place implements Listener {
    private RefinedStorage refinedStorage;

    public Place(RefinedStorage refinedStorage){
        this.refinedStorage = refinedStorage;
    }
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event){
        if(refinedStorage.getNbt().isCustomItem(event.getItemInHand(), "number")){
            int number = refinedStorage.getNbt().getInt(event.getItemInHand(), "number");
            Block block = event.getBlockPlaced();
            LocationData locationData = new LocationData(block.getX(), block.getY(), block.getZ(), block.getWorld().getName());
            refinedStorage.getDataManager().addController(new ControllerData(number, new ArrayList(), SortType.NONE, locationData));
        }
    }
}
