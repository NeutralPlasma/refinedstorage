package eu.virtusdevelops.refinedstorage.listeners;

import eu.virtusdevelops.refinedstorage.RefinedStorage;
import eu.virtusdevelops.refinedstorage.data.ControllerData;
import eu.virtusdevelops.refinedstorage.data.LocationData;
import eu.virtusdevelops.refinedstorage.managers.DataManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class Break implements Listener {
    private DataManager dataManager;
    private RefinedStorage refinedStorage;

    public Break(RefinedStorage refinedStorage){
        this.refinedStorage = refinedStorage;
        this.dataManager = refinedStorage.getDataManager();
    }
    /*
        TODO:
        Add protection plugins hooks.
    */

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        Block block = event.getBlock();
        Location location = block.getLocation();
        ControllerData controllerData = dataManager.getController(new LocationData(block.getX(), block.getY(), block.getZ(), block.getWorld().getName()));
        if(controllerData != null){
            ItemStack item = new ItemStack(Material.DISPENSER);
            item = refinedStorage.getNbt().createControllerItem(item, controllerData);
            location.getWorld().dropItemNaturally(location, item);
        }else{
            ItemStack item = new ItemStack(Material.DISPENSER);
            item = refinedStorage.getNbt().createControllerItem(item, 1);
            location.getWorld().dropItemNaturally(location, item);
        }
    }
}
