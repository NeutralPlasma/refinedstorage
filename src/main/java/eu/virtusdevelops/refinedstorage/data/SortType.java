package eu.virtusdevelops.refinedstorage.data;

public enum SortType {
    ITEM_AMOUNT, ITEM_NAME, NONE;
}
