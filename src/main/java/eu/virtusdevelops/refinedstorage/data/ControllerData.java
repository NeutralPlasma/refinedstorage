package eu.virtusdevelops.refinedstorage.data;


import java.util.List;

public class ControllerData {
    private int number;
    private List<String> links;
    private SortType sortType;
    private LocationData locationData;

    public ControllerData(int number, List<String> links, SortType type, LocationData locationData){
        this.number = number;
        this.links = links;
        this.sortType = type;
        this.locationData = locationData;
    }

    public List<String> getLinks() {
        return links;
    }

    public int getNumber() {
        return number;
    }

    public LocationData getLocationData() {
        return locationData;
    }

    public SortType getSortType() {
        return sortType;
    }

    // Set stuff

    public void setLinks(List<String> links) {
        this.links = links;
    }

    public void setLocationData(LocationData locationData) {
        this.locationData = locationData;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }
}
