package eu.virtusdevelops.refinedstorage.data;

public class LocationData {
    private int x;
    private int y;
    private int z;
    private String world;

    public LocationData(int x, int y, int z, String world){
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
    }

    public int getZ() {
        return z;
    }

    public int getY() {
        return y;
    }
    public int getX() {
        return x;
    }

    public String getWorld() {
        return world;
    }

    @Override
    public String toString(){
        return x + ":" + y + ":" + z + ":" + world;
    }
}
