package eu.virtusdevelops.refinedstorage.data;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

public class DiskData {
    private int number;
    private UUID id;
    private ArrayList<ItemStack> items;

    public DiskData(int number, UUID uuid, ArrayList items){
        this.number = number;
        this.id = uuid;
        this.items = items;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public ArrayList<ItemStack> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemStack> items) {
        this.items = items;
    }

    public void addItem(ItemStack item){
        this.items.add(item);
    }
}
