package eu.virtusdevelops.refinedstorage.commands;

import eu.virtusdevelops.refinedstorage.RefinedStorage;
import eu.virtusdevelops.virtuscore.command.AbstractCommand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class GiveCommand extends AbstractCommand {
    private RefinedStorage refinedStorage;

    public GiveCommand(RefinedStorage refinedStorage){
        super(CommandType.BOTH, true, "give");
        this.refinedStorage = refinedStorage;
    }

    @Override
    protected ReturnType runCommand(CommandSender sender, String... args) {
        if(args.length < 1){
            return ReturnType.SYNTAX_ERROR;
        }

        Player player = Bukkit.getPlayer(args[0]);
        if(player == null){
            return ReturnType.SYNTAX_ERROR;
        }

        if (args.length < 1) {
            return ReturnType.SYNTAX_ERROR;
        }
        int level = 1;
        int amount = 1;

        try {
            if (args.length > 1) { // level of the controller
                level = Integer.parseInt(args[1]);
            }
        }catch (NumberFormatException error){
            return ReturnType.SYNTAX_ERROR;
        }
        try {
            if (args.length > 2) {
                amount = Integer.parseInt(args[2]);
            }
        }catch (NumberFormatException error){
            return ReturnType.SYNTAX_ERROR;
        }

        ItemStack item = new ItemStack(Material.DISPENSER);
        item = refinedStorage.getNbt().createControllerItem(item, level);
        item.setAmount(amount);
        player.getInventory().addItem(item);


        return ReturnType.SUCCESS;
    }

    @Override
    protected List<String> onTab(CommandSender commandSender, String... strings) {
        return null;
    }

    @Override
    public String getPermissionNode() {
        return "refinedstorage.command.give";
    }

    @Override
    public String getSyntax() {
        return "give <player> <id> [amount]";
    }

    @Override
    public String getDescription() {
        return "Give specific person specific item.";
    }
}
