package eu.virtusdevelops.refinedstorage.managers;

import com.google.gson.Gson;
import eu.virtusdevelops.refinedstorage.RefinedStorage;
import eu.virtusdevelops.refinedstorage.data.ControllerData;
import eu.virtusdevelops.refinedstorage.data.DiskData;
import eu.virtusdevelops.refinedstorage.data.LocationData;
import eu.virtusdevelops.refinedstorage.storage.DataStorage;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashMap;
import java.util.UUID;

public class DataManager {
    private RefinedStorage refinedStorage;
    private Gson gson;
    private DataStorage dataStorage;
    private HashMap<String, ControllerData> controllerList = new HashMap<>();
    private HashMap<UUID, DiskData> diskList = new HashMap<>();

    public DataManager(RefinedStorage refinedStorage){
        dataStorage = refinedStorage.getDataStorage();
        this.gson = new Gson();
        this.refinedStorage = refinedStorage;
        setup();
        start();
    }

    /*
        Controller data setup
     */
    public void addController(ControllerData info){
        dataStorage.getData().set("controllers." + info.getLocationData().toString(), gson.toJson(info));
        controllerList.put(info.getLocationData().toString(), info);
    }

    public ControllerData getController(LocationData info){
        return controllerList.get(info.toString());
    }

    public void updateController(ControllerData info){
        removeController(info.getLocationData());
        addController(info);
    }

    public void removeController(LocationData info){
        dataStorage.getData().set("controllers." + info.toString(), null);
        controllerList.remove(info.toString());
    }

    public void setup(){
        try {
            ConfigurationSection configurationSection = dataStorage.getData().getConfigurationSection("controllers");
            for (String entered : configurationSection.getKeys(true)) {

                String path = dataStorage.getData().getString("controllers." + entered);

                ControllerData controllerData = gson.fromJson(path, ControllerData.class);
                if (controllerData != null) {
                    controllerList.put(controllerData.getLocationData().toString(), controllerData);
                }

            }
            ConfigurationSection configurationSection2 = dataStorage.getData().getConfigurationSection("disks");
            for (String entered : configurationSection2.getKeys(true)) {

                String path = dataStorage.getData().getString("disks." + entered);

                DiskData diskData = gson.fromJson(path, DiskData.class);
                if (diskData != null) {
                    diskList.put(diskData.getId(), diskData);
                }

            }
        }catch (Exception ignored) {}
    }

    /*
        Disk Data setup.
     */

    public void addDisk(DiskData info){
        dataStorage.getData().set("disks." + info.getId().toString(), gson.toJson(info));
        diskList.put(info.getId(), info);
    }

    public DiskData getDisk(UUID info){
        return diskList.get(info);
    }

    public void updateDisk(DiskData info){
        removeDisk(info.getId());
        addDisk(info);
    }

    public void removeDisk(UUID info){
        dataStorage.getData().set("disks." + info.toString(), null);
        diskList.remove(info);
    }

    /*
        SAVEDATA EVERY SOMETIME
    */

    public void start(){
        Bukkit.getScheduler().runTaskTimerAsynchronously(refinedStorage, () -> {
            dataStorage.saveData();
        }, 20L, 600L);
    }
}
