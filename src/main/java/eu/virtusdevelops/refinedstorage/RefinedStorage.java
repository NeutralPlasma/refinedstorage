package eu.virtusdevelops.refinedstorage;

import eu.virtusdevelops.refinedstorage.commands.GiveCommand;
import eu.virtusdevelops.refinedstorage.listeners.Break;
import eu.virtusdevelops.refinedstorage.listeners.Place;
import eu.virtusdevelops.refinedstorage.managers.DataManager;
import eu.virtusdevelops.refinedstorage.storage.DataStorage;
import eu.virtusdevelops.refinedstorage.util.NBT.NBT;
import eu.virtusdevelops.virtuscore.VirtusCore;
import eu.virtusdevelops.virtuscore.command.CommandManager;
import eu.virtusdevelops.virtuscore.managers.FileManager;
import eu.virtusdevelops.virtuscore.utils.FileLocation;
import eu.virtusdevelops.virtuscore.utils.TextUtil;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class RefinedStorage extends JavaPlugin {


    private FileManager fileManager;
    private DataStorage dataStorage;
    private DataManager dataManager;
    private CommandManager commandManager;
    private NBT nbt;

    @Override
    public void onEnable() {
        long time = System.currentTimeMillis();
        String line = "&a=================================";
        String messageline1 = "&6Made by Virtus Develops";
        String messageline2 = "&7RefinedStorage: &6" + this.getDescription().getVersion();
        String messageline3 = "&7Running on: " + getServer().getVersion();
        String messageline4 = "&7Action: &aEnabling plugin&7...";
        VirtusCore.console().sendMessage(TextUtil.colorFormat(line));
        VirtusCore.console().sendMessage(TextUtil.colorFormat(messageline1));
        VirtusCore.console().sendMessage(TextUtil.colorFormat(messageline2));
        VirtusCore.console().sendMessage(TextUtil.colorFormat(messageline3));
        VirtusCore.console().sendMessage(TextUtil.colorFormat(messageline4));

        saveDefaultConfig();

        this.nbt = new NBT(this);

        this.fileManager = new FileManager(this, new LinkedHashSet<>(Arrays.asList(
                FileLocation.of("configuration/disks.yml", true, true),
                FileLocation.of("configuration/items.yml", true, true)
        )));
        this.fileManager.loadFiles();
        this.dataStorage = new DataStorage(this);
        this.dataStorage.setup();
        this.dataManager = new DataManager(this);
        this.commandManager = new CommandManager(this);
        /*
            Command register
         */
        registerCommands();

        /*
            Register events
         */
        VirtusCore.plugins().registerEvents(new Break(this), this);
        VirtusCore.plugins().registerEvents(new Place(this), this);


        time = System.currentTimeMillis() - time;
        String lineend = "&a===============[&b" + time + "ms &a]=================";
        VirtusCore.console().sendMessage(TextUtil.colorFormat(lineend));
    }

    @Override
    public void onDisable() {
        this.fileManager.clear();

    }

    public FileManager getFileManager(){
        return this.fileManager;
    }

    public NBT getNbt() {
        return nbt;
    }

    public DataStorage getDataStorage() {
        return dataStorage;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    private void registerCommands() {

        commandManager.addMainCommand("refinedstorage").addSubCommands(
            new GiveCommand(this)
        );


    }
}
